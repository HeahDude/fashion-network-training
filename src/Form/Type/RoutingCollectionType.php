<?php

namespace App\Form\Type;

use App\Form\RoutingCollectionItemType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoutingCollectionType extends AbstractType
{
    public function getParent()
    {
        return CollectionType::class;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('entry_type', RoutingCollectionItemType::class);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addModelTransformer(new CallbackTransformer(
                function ($data) {
                    if (!$data) {
                        return [];
                    }

                    if (!is_array($data)) {
                        throw new TransformationFailedException('Expected an array');
                    }

                    foreach ($data as $key => $value) {
                        $values[$key] = ['key' => $key, 'value' => $value];
                    }

                    return $values ?? [];
                },
                function ($value) {
                    if (!$value) {
                        return [];
                    }

                    if (!is_array($value)) {
                        throw new TransformationFailedException('Expected an array');
                    }

                    foreach ($value as $key => $item) {
                        $data[$key] = $item['value'];
                    }

                    return $data ?? [];
                }
            ))
        ;
    }
}
