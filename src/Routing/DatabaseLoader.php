<?php

namespace App\Routing;

use App\Entity\Route as RouteEntity;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class DatabaseLoader extends Loader
{
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function load($resource, $type = null)
    {
        switch ($resource) {
            case RouteEntity::class:
                $routes = new RouteCollection();

                /** @var RouteEntity $routeEntity */
                foreach ($this->doctrine->getRepository($resource)->findAll() as $routeEntity) {
                    $routes->add($routeEntity->getName(), new Route(
                        $routeEntity->getPath(),
                        $routeEntity->getAttributes(),
                        $routeEntity->getRequirements(),
                        [],
                        $routeEntity->getHost(),
                        [],
                        array_map('trim', explode(',', $routeEntity->getMethods())),
                        $routeEntity->getCondition()
                    ));
                }

                // Add your routes based on the resource or the type, or both.
                // You can even load routes from the database, but be careful!
                // If you do so, you will need to clear the cache each time routes change!
                return $routes;

                break;

            default:
                throw new \LogicException('Not able to load :'.$resource);
        }
    }

    public function supports($resource, $type = null)
    {
        // You can certain resources or certain type. For instance, $resource could
        // be a file, an array or anything else, depending on the app using this loader.
        return 'app_entity' === $type && null !== $this->doctrine->getManagerForClass($resource);
    }
}
