<?php

namespace App\Translation;

use Symfony\Contracts\Translation\TranslatorInterface;

class HomepageTranslator
{
    private const DOMAIN = 'homepage';

    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function trans(string $id, array $parameters = array(), string $locale = null): string
    {
        return $this->translator->trans($id, $parameters, self::DOMAIN, $locale);
    }
}
