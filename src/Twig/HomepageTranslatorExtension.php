<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class HomepageTranslatorExtension extends AbstractExtension
{
    public function getFilters()
    {
        return array(
            new TwigFilter('homepage_trans', [HomepageTranslatorRuntime::class, 'homepageTrans'])
        );
    }
}
