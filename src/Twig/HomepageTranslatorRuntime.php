<?php

namespace App\Twig;

use App\Translation\HomepageTranslator;
use Twig\Extension\AbstractExtension;
use Twig\Extension\RuntimeExtensionInterface;
use Twig\TwigFilter;

class HomepageTranslatorRuntime implements RuntimeExtensionInterface
{
    private $translator;

    public function __construct(HomepageTranslator $translator)
    {
        $this->translator = $translator;
    }

    public function homepageTrans($key, array $parameters = [], string $locale = null): string
    {
        return $this->translator->trans($key, $parameters, $locale);
    }
}
